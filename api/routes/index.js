"use strict";
module.exports = function(app) {
  var InvoiceController = require("../controllers/InvoiceController");

  var Keycloak = require("keycloak-connect");
  var session = require("express-session");
  var memoryStore = new session.MemoryStore();
  app.use(
    session({
      secret: "mySecret",
      resave: false,
      saveUninitialized: true,
      store: memoryStore
    })
  );

  let kcConfig = {
    bearerOnly: true,
    realm: process.env.KEYCLOAK_REALM,
    "auth-server-url": process.env.KEYCLOAK_URL,
    "ssl-required": "external",
    resource: "microservice"
  };

  let keycloak = new Keycloak({ store: memoryStore }, kcConfig);

  app.use(keycloak.middleware());

  app.use(
    keycloak.middleware({
      logout: "/logout",
      admin: "/"
    })
  );
  app
    .route("/v1/escounts-ms/api/invoiceData")
    .get(InvoiceController.invoiceData);
};

"use strict";

module.exports = function(app) {
  app.route("/v1/escounts-ms/ping").get((req, res) => {
    res.send({
      status: "OK",
      message: "pong"
    });
  });
};

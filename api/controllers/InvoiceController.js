"use strict";

var fs = require("fs");
var async = require("async");
var _ = require("lodash");
const moment = require("moment");
const MomentRange = require("moment-range");
const moment_range = MomentRange.extendMoment(moment);
let AWS = require("aws-sdk");
AWS.config.update({
  credentials: new AWS.Credentials(
    process.env.AWS_ACCESS_KEY,
    process.env.AWS_SECRET_KEY
  ),
  region: process.env.ES_REGION
});
let options = {
  hosts: [process.env.ES_ENDPOINT],
  connectionClass: require("http-aws-es"),
  awsConfig: new AWS.Config({ region: process.env.ES_REGION })
};

const client = require("elasticsearch").Client(options);

module.exports = {
  invoiceData: function(req, res) {
    let companyKeys = req.body.DocNumber ? req.body.DocNumber : "";
    client.search(
      {
        index: "qb_invoice_clone",
        type: "qb_invoice_clone_type",
        body: {
          query: {
            match: {
              "DocNumber.keyword": companyKeys
            }
          }
        }
      },
      function(error, response, status) {
        if (error) {
          console.log("search error: " + error);
          res.status(500).json({
            error: error
          });
        } else {
          var totalData = response.hits.hits;
          res.json({
            totalData: totalData
          });
        }
      }
    );
  }
};

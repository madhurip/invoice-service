# pricing-service 
Service with node, express, docker and kafka-node. 
 

## Prerequisites
1. Configure [AWS CLI](https://slack-files.com/T3DBSSN8P-F6BPNCFTM-3d2d925364). 
2. Install PM2 globally. `npm install pm2 -g`
3. Node version required: `^8.2.0`

## Setup linting
Read [this document](https://slack-files.com/T3DBSSN8P-F6BPNCFTM-3d2d925364) to set up linting in Sublime (or) any favorite Editor.

## Setup
1. Clone this repository.
2. Run `npm install`
3. Create your feature branch and start working on it.`Recommended: feature_truckstatus`

## Development environment
1. Add `.env` file to the local code
2. To start Development environment use `NODE_ENV=production PORT=3003 node server.js`
3. To stop use `Ctrl + C`

## Logging
1. We are using `winston` and `morgon` for logging
2. Please read [this link](http://www.jyotman.xyz/post/logging-in-node.js-done-right) before proceeding furthur.
3. All server logs can be seen in cloud-watch in AWS, in the foleder `SERVICE_NAME-service`/`Example: pricing-services`
4. Deployment automatically creates a log folder in AWS Cloudwatch. You can into your AWS account and check the logs.
5. Always use `logger.log` instead of `console.log` 

## How to use Authentication for your Routes
* We are using keycloak (Enterprise level auth platfrom) for all auth purposes. [Link](https://keycloak.gitbooks.io/documentation/authorization_services/index.html) 


## How to build Service Docker?
1. Clone the repo
2. Check if it runs locally without using docker
3. Build image from code using `docker build -t <Image Name> .`
4. Run the image using `docker run -it -p 3000:3000 -e PORT=3000 -e AWS_ACCESS_KEY="accesskey" -e AWS_SECRET_KEY="secretkey" --name <container name you want> <Image Name>` 

## Using persistant DB for your service

We are using DynamoDB for all nosql purposes.

## Kafka - Event driven infrastructure

At the heart of our entire infrastructure we have Kafka to which you can read and write. We are using `kafka-node` module for integration. 
Read more about how to connect kafka to our system [here](https://www.npmjs.com/package/kafka-node).

## API Guidelines
1. Always implement `/ping` api for every microservice so that we can configure health checks on this url. (`PingRoute.js` is already implemented. Please dont edit or delete this)

## Caching locally within your service

We are currently using redis as caching mechanism for all the services.

## Shared file system for your services (docker volumes)

All ECS instances have EFS integrated to it and mounted on `/efs`. 



## This repository uses two tutorials for setting up this:
1. https://www.codementor.io/olatundegaruba/nodejs-restful-apis-in-10-minutes-q0sgsfhbd
2. https://scotch.io/tutorials/test-a-node-restful-api-with-mocha-and-chai

<h1>envfile</h1>

PORT=  
AWS_ACCESS_KEY=""   
AWS_SECRET_KEY=""   
AWS_REGION=""  
KAFKA_CLIENT=""   
KAFKA_PRODUCER=""  
KEYCLOAK_SECURITY=""  
KEYCLOAK_REALM = ""   
KEYCLOAK_URL = ""  
BASE_URL = ""  
GRANT_TYPE = ""  
KC_USERNAME = ""  
KC_PASSWORD = ""   
CLIENT_ID = ""   
REALM = ""    



Update the .env details locally

In docker file for production:
global install pm2
RUN npm start

## Primary Point Of Contacts

 **Raj**  ` =>(rajkuamr@transin.in)`  
  **Saiteja**  ` =>(teja@transin.in)`  
 
## Seconder Point Of Contacts
 
  **Mahesh** ` =>(mahesh@transin.in)`    
  **Rajasekher** ` =>(rajasekhar@transin.in)`
  

  
## API_Doc's Postman(or)swagger url:  
https://www.getpostman.com/collections/98bf36001c4117c06c2d